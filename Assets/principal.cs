﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class principal : MonoBehaviour {

    public GameObject jogador;
    public GameObject idle;
    public GameObject bate;
    public GameObject barril;
    public GameObject barrilEsquerdo;
    public GameObject barrilDireito;
    public Text pontuacao;
    private int score;
    private bool ladoJogador;
    private bool acabou;
    private bool comecou;
    private float escalaJogadorHorizontal;
    public AudioClip sombate;
    public AudioClip somperde;
    public GameObject barra;
    private List<GameObject> listaBlocos;
    
    // Use this for initialization
    void Start () {
        escalaJogadorHorizontal = transform.localScale.x;
        bate.SetActive(false);
        listaBlocos = new List<GameObject>();
        CriarBarrisInicio();
        pontuacao.transform.position = new Vector2(Screen.width / 2, Screen.height /2 + 120);
        pontuacao.text = "Toque para iniciar";
        pontuacao.fontSize = 25;
    }
	
	// Update is called once per frame
	void Update () {
        if (!acabou)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!comecou)
                {
                    comecou = true;
                    barra.SendMessage("Comecou");
                }

                GetComponent<AudioSource>().PlayOneShot(sombate);

                if (Input.mousePosition.x > Screen.width / 2)
                {
                    bateDireita();
                }
                else
                {
                    bateEsquerda();
                }

                listaBlocos.RemoveAt(0);
                confereJogada();
                ReposicionarBlocos();
            }
        }
	}

    void bateDireita()
    {
        ladoJogador = true;
        idle.SetActive(false);
        bate.SetActive(true);
        jogador.transform.position = new Vector2(1.1f, jogador.transform.position.y);
        jogador.transform.localScale = new Vector2(-escalaJogadorHorizontal, jogador.transform.localScale.y);
        Invoke("voltarAnimacao", 0.25f);
        listaBlocos[0].SendMessage("animaDireita");

    }

    void bateEsquerda()
    {
        ladoJogador = false;
        idle.SetActive(false);
        bate.SetActive(true);
        jogador.transform.position = new Vector2(-1.1f, jogador.transform.position.y);
        jogador.transform.localScale = new Vector2(escalaJogadorHorizontal, jogador.transform.localScale.y);
        Invoke("voltarAnimacao", 0.25f);
        listaBlocos[0].SendMessage("animaEsquerda");
    }

    void voltarAnimacao()
    {
        idle.SetActive(true);
        bate.SetActive(false);
    }

    GameObject CriarNovoBarril(Vector2 posicao)
    {
        GameObject novoBarril; 
        if(Random.value > 0.5f || (listaBlocos.Count <2))
        {
            novoBarril = Instantiate(barril);
        }
        else
        {
            if (Random.value > 0.5f)
            {
                novoBarril = Instantiate(barrilDireito);
            }
            else
            {
                novoBarril = Instantiate(barrilEsquerdo);
            }                           
        }
        novoBarril.transform.position = posicao;

        return novoBarril;
    }


    void CriarBarrisInicio()
    {
        for(int i=0; i<=7; i++)
        {
            GameObject bar = CriarNovoBarril(new Vector2(0, -2.12f + (i*0.99f) ) );
            listaBlocos.Add(bar);

        }
    }

    void ReposicionarBlocos()
    {
        GameObject bar = CriarNovoBarril(new Vector2(0, -2.12f + (7 * 0.99f)));
        listaBlocos.Add(bar);
        for (int i = 0; i < 7; i++)
        {
            listaBlocos[i].transform.position = new Vector2(listaBlocos[i].transform.position.x, listaBlocos[i].transform.position.y-0.99f);
        }
    }


    void confereJogada()
    {
        if (listaBlocos[0].gameObject.CompareTag("Inimigo"))
        {
            if( (listaBlocos[0].name == "inimigoEsq(Clone)" && !ladoJogador || listaBlocos[0].name == "inimigoDir(Clone)" && ladoJogador) )
            {
                FimJogo();
            }
            else
            {
                MarcarPonto();
            }
        }
        else
        {
            MarcarPonto();
        }
    }

    void MarcarPonto()
    {
        score++;
        pontuacao.text = score.ToString();

        pontuacao.fontSize = 100;
        pontuacao.color = new Color(0.95f, 1.0f, 0.35f);
        barra.SendMessage("AumentarBarra");
    }

    void FimJogo()
    {
        acabou = true;
        idle.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);
        bate.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);


        jogador.GetComponent<Rigidbody2D>().isKinematic = false;
        


        if (ladoJogador)
        {
            jogador.GetComponent<Rigidbody2D>().AddTorque(-100f);
            jogador.GetComponent<Rigidbody2D>().velocity = new Vector2(5.0f, 3.0f);
        }
        else
        {
            jogador.GetComponent<Rigidbody2D>().AddTorque(100f);
            jogador.GetComponent<Rigidbody2D>().velocity = new Vector2(-5.0f, 3.0f);
        }
        GetComponent<AudioSource>().PlayOneShot(somperde);

        Invoke("RecarregarCena", 2);
    }

    void RecarregarCena()
    {
        Application.LoadLevel("minhacena");
    }
}
